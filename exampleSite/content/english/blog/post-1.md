---
title: "Introduction"
image: "images/blog/intro-resize.png"
description: "This is meta description."
draft: false
---

This is an introduction to your project.  Answer the following.

1. Our 'Why'

We decided to make this project for the love of the game.  We wanted to blast David's tunes a little louder so that the whole class could enjoy it in all its fullness.  

2. What we learned

To make websites.  To use our physics knowledge to create real world solutions.  And most importantly, as Yoda once said: "do or do not, there is no try."

3. Difficulties / challenges

At first we had trouble agreeing on what project we were going to do. After that, the coding was a bit difficult.  In the end, we were able to have a finished product that we were happy with.

